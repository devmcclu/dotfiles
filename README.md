# .dotfiles
My Dotfiles for Linux systems

+ **WM**: [LeftWM](http://leftwm.org/)
+ **OS**: Arch Linux
+ **Shell**: [fish](https://wiki.archlinux.org/index.php/fish)
+ **Terminal**: [kitty](https://github.com/kovidgoyal/kitty/) config included!
+ **Editor**: [VSCode](https://code.visualstudio.com/)
+ **Launcher**: [rofi](https://github.com/davatorium/rofi/) config included!
+ **Browser**: Firefox
